package com.ciezki.jeppesen.zadanie;

import com.ciezki.jeppesen.zadanie.calculator.Division;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class DivisionTest {

    //    dokumentacja biblioteki junit mowi ze lepiej wykorzystac nowa metode assertEquals
    //    z tolerancja okreslana przez "delte"-jednak nie mam 100% pewnosci czy dobrze ja uzylem.


    @Test
    public void shouldDivideTwoPositiveNumbers() {
        Division division = new Division();
        double result = division.compute(2.5, 10.5);
        assertEquals(0.2380, result, 1e-8);
    }

    @Test
    public void shouldDividePositiveAndNegativeNumbers() {
        Division division = new Division();
        double result = division.compute(-2.5,10.5);
        assertEquals(-0.2380,result,1e-8);
    }
    @Test
    public void shouldDivideTwoNegativeNumbers(){
        Division division = new Division();
        double result = division.compute(-2.5,-10.5);
        assertEquals(0.2380,result,1e-8);
    }

    @Test
    public void shouldGetErrorMessageIfDivideByZero (){
        try{
            Division division = new Division();
            division.compute(5,0);
            fail ("Exception wasn't thrown");
        } catch (ArithmeticException e){
            assertEquals("Nie dziel przez 0!", e.getMessage());
        }
    }


}
