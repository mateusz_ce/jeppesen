package com.ciezki.jeppesen.zadanie;

import com.ciezki.jeppesen.zadanie.calculator.Multiplication;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MultiplicationTest {

    @Test
    public void shouldMultiplyTwoPositiveNumbers() {
        Multiplication multiplication = new Multiplication();
        double result = multiplication.compute(5.5, 5.5);
        assertEquals(30.25, result, 1e-8);
    }

    @Test
    public void shouldMultiplyPositiveAndNegativeNumber() {
        Multiplication multiplication = new Multiplication();
        double result = multiplication.compute(-5.5, 5.5);
        assertEquals(-30.25, result, 1e-8);
    }

    @Test
    public void shouldMultiplytwoNegativeNumbers() {
        Multiplication multiplication = new Multiplication();
        double result = multiplication.compute(-5.5, -5.5);
        assertEquals(30.25, result, 1e-8);
    }


}
