package com.ciezki.jeppesen.zadanie;

import com.ciezki.jeppesen.zadanie.calculator.Calculator;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class CalculatorTest {


    @Test
    public void testIsDivisionMethod() {
        Calculator calculator = new Calculator();
        assertTrue(calculator.isDivision("/"));
        assertFalse(calculator.isDivision(":"));
    }
    @Test
    public void testIsAdditionMethod() {
        Calculator calculator = new Calculator();
        assertTrue(calculator.isAddition("+"));
        assertFalse(calculator.isAddition("++"));
    }

    @Test
    public void testIsSubtractionMethod() {
        Calculator calculator = new Calculator();
        assertTrue(calculator.isSubtraction("-"));
        assertFalse(calculator.isSubtraction("a"));
    }

    @Test
    public void testIsMultiplicationMethod() {
        Calculator calculator = new Calculator();
        assertTrue(calculator.isMultiplication("*"));
        assertFalse(calculator.isMultiplication("a"));
    }
}
