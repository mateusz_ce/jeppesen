package com.ciezki.jeppesen.zadanie;

import com.ciezki.jeppesen.zadanie.calculator.Subtraction;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SubtractionTest {


    @Test
    public void shouldSubtractTwoPositiveNumbers() {
        Subtraction subtraction = new Subtraction();
        double result = subtraction.compute(10.3, 0.5);
        assertEquals(9.8, result, 1e-8);
    }

    @Test
    public void shouldSubtractPositiveAndNegativeNumbers() {
        Subtraction subtraction = new Subtraction();
        double result = subtraction.compute(5.5, -5.5);
        assertEquals(11, result, 1e-8);
    }

    @Test
    public void shouldSubtractTwoNegativeNumbers() {
        Subtraction subtraction = new Subtraction();
        double result = subtraction.compute(-5.5, -5.5);
        assertEquals(0, result, 1e-8);
    }

}
