package com.ciezki.jeppesen.zadanie.calculator;

public class Subtraction implements Calculations {
    @Override
    public double compute (double numberA, double numberB){
        return numberA - numberB;
    }
}
