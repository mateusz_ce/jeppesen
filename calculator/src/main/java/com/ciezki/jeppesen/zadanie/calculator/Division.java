package com.ciezki.jeppesen.zadanie.calculator;

public class Division implements Calculations {
    @Override
    public double compute(double numberA, double numberB) {
        try {
            if (numberA != 0 && numberB != 0) {
                return numberA / numberB;
            } else {
                throw new ArithmeticException();
            }
        } catch (ArithmeticException e) {
            System.out.println("Nie dziel przez 0!");
        }
        return 0;
    }
}

