package com.ciezki.jeppesen.zadanie.calculator;

public interface Calculations {
    double compute(double numberA, double numberB);
}

