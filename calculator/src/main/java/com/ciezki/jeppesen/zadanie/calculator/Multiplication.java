package com.ciezki.jeppesen.zadanie.calculator;

public class Multiplication implements Calculations{
    @Override
    public double compute (double numberA, double numberB){
        return numberA * numberB;
    }
}
