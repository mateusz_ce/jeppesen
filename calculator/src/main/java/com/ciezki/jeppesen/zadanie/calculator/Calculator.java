package com.ciezki.jeppesen.zadanie.calculator;

import java.util.Optional;
import java.util.Scanner;

public class Calculator {
//    UWAGI:
//    1. Projekt postawiony na spring initializer poniewaz na poczatku chcialem zrobic web;
//    2. Staralem sie uzyc wzorca strategii aczkolwiek wydaje i sie ze nie jest to ksiazkowy jej przyklad;
//    3. W testach - uwaga apropo biblioteki i metody assertEqueals;
//    4. Przez brak specyfikacji na temat ostatecznego wygladu kalklatora postawilem na proste rozwiazania; Chcialem uzyc algorytmu ONP jednakze zabraklo mi czasu...


    public static void main(String[] args) throws Exception {
        Calculator calculator = new Calculator();
        calculator.startCalculator();
    }


    private void startCalculator() {
        try (Scanner keyBoardInputScanner = new Scanner(System.in)) {
            Calculations calculation = readOperation(keyBoardInputScanner);
            Double firstNumber = readNumber(keyBoardInputScanner);
            Double secondNumber = readNumber(keyBoardInputScanner);
            double result = calculation.compute(firstNumber, secondNumber);

            System.out.println("Wynik to: " + result);
        }
    }

    //    odczyt liczb
    private Double readNumber(Scanner keyBoardInputScanner) {
        Optional<Double> number = Optional.empty();
        String keyboardInput;
        while (!number.isPresent()) {
            System.out.println("Podaj liczbe");
            keyboardInput = keyBoardInputScanner.next();
            number = parseAndValidateInputNumber(keyboardInput);
        }
        return number.get();
    }

    //odczyt operatora
    private Calculations readOperation(Scanner keyBoardInputScanner) {
        Optional<Calculations> inputOperation = Optional.empty();
        String keyboardInput;
        while (!inputOperation.isPresent()) {
            System.out.println("Wybierz operacje: \n" +
                    "-dodawanie: + \n" +
                    "-mnozenie: * \n" +
                    "-odejmowanie: - \n" +
                    "-dzielenie: /");
            keyboardInput = keyBoardInputScanner.next();
            inputOperation = chooseCalculation(keyboardInput);
        }
        return inputOperation.get();
    }

    //wybor operacji
    private Optional<Calculations> chooseCalculation(String input) {
        if ("X".equals(input)) {
            System.exit(0);
        } else if (isAddition(input)) {
            return Optional.of(new Addition());
        } else if (isSubtraction(input)) {
            return Optional.of(new Subtraction());
        } else if (isMultiplication(input)) {
            return Optional.of(new Multiplication());
        } else if (isDivision(input)) {
            return Optional.of(new Division());
        }
        System.out.println("Niepoprawna wartosc \"X\" zeby wyjsc");
        return Optional.empty();
    }

    public boolean isDivision(String input) {
        return "/".equals(input);
    }

    public boolean isMultiplication(String input) {
        return "*".equals(input);
    }

    public boolean isSubtraction(String input) {
        return "-".equals(input);
    }

    public boolean isAddition(String input) {
        return "+".equals(input);
    }

    //validacja wprowadzonych liczb
    private Optional<Double> parseAndValidateInputNumber(String input) {
        if ("X".equals(input)) {
            System.exit(0);
        }
        try {
            return Optional.of(Double.valueOf(input));
        } catch (NumberFormatException e) {
            System.out.println("Niepoprawna wartosc \"X\" zeby wyjsc");
            return Optional.empty();
        }
    }
}
